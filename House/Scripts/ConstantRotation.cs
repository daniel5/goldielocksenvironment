﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public float rotationSpeed = 10f;
    private Transform t;

    void Start()
    {
        t = GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        Vector3 r = t.rotation.eulerAngles;
        r.y += rotationSpeed * Time.fixedDeltaTime;
        t.rotation = Quaternion.Euler(r);
    }
}