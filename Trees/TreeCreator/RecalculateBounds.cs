﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class RecalculateBounds : MonoBehaviour
{
    public bool recalulateBounds = false;


    // Update is called once per frame
    void Update()
    {
        if (recalulateBounds)
        {
            recalulateBounds = false;
            GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();
        }
    }
}
