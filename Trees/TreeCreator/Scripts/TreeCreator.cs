﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class TreeCreator : MonoBehaviour
{
    private Mesh mesh;
    public bool generate = false;
    public GameObject leaf;
    public float randomOffset = 1f;

    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    private void Update()
    {
        if (generate)
        {
            generate = false;
            var points = new List<Vector3>();
            var leafs = new List<GameObject>();


            foreach (var vert in mesh.vertices)
            {
                if (!points.Contains(vert))
                {
                    points.Add(vert);
                    var leafGO = Instantiate(leaf, transform.TransformPoint(vert), Quaternion.identity);
                    leafs.Add(leafGO);
                    var pos = leafGO.transform.position;
                    pos.x += Random.Range(-randomOffset / 2f, randomOffset / 2f);
                    pos.y += Random.Range(-randomOffset, randomOffset);
                    pos.z += Random.Range(-randomOffset / 2f, randomOffset / 2f);
                    leafGO.transform.position = pos;
                    leafGO.transform.LookAt(Camera.main.transform);
                    var euler = leafGO.transform.eulerAngles;
                    euler.z = Random.Range(0f, 360f);
                    leafGO.transform.rotation = Quaternion.Euler(euler);
                }
            }

            CombineInstance[] combine = new CombineInstance[leafs.Count];

            int x = 0;
            while (x < leafs.Count)
            {
                combine[x].mesh = leafs[x].GetComponent<MeshFilter>().sharedMesh;
                combine[x].transform = leafs[x].transform.localToWorldMatrix;
                DestroyImmediate(leafs[x]);
                x++;
            }

            GameObject finalMesh = GameObject.CreatePrimitive(PrimitiveType.Cube);
            finalMesh.name = "FinalLeafCone";
            finalMesh.GetComponent<MeshFilter>().mesh = new Mesh();
            finalMesh.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            finalMesh.GetComponent<MeshRenderer>().sharedMaterials = leaf.GetComponent<MeshRenderer>().sharedMaterials;
            GameObject original = GameObject.CreatePrimitive(PrimitiveType.Cube);
            original.name = "originalAtZero";
            var originalCopy = Instantiate(GetComponent<MeshFilter>().mesh);
            originalCopy.name = "OriginalCopy";
            var copied = originalCopy.vertices;
            for (int i = 0; i < originalCopy.vertices.Length; i++)
            {
                copied[i] = transform.TransformPoint(originalCopy.vertices[i]);
            }

            originalCopy.vertices = copied;
            original.GetComponent<MeshFilter>().mesh = originalCopy;

            var finalMeshNormals = finalMesh.GetComponent<MeshFilter>().sharedMesh.normals;
            var finalMeshVertices = finalMesh.GetComponent<MeshFilter>().sharedMesh.vertices;
            for (int i = 0; i < finalMeshNormals.Length; i++)
            {
                finalMeshNormals[i] = getClosestNormalFromMesh(finalMeshVertices[i], originalCopy);
            }

            finalMesh.GetComponent<MeshFilter>().sharedMesh.normals = finalMeshNormals;
            DestroyImmediate(original);
            gameObject.SetActive(false);
            finalMesh.GetComponent<MeshFilter>().sharedMesh.vertices = transformMesh(
                finalMesh.GetComponent<MeshFilter>().sharedMesh, -transform.position, Quaternion.identity, Vector3.one);
            finalMesh.transform.position = transform.position;
        }
    }

    public static Vector3[] transformMesh(Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        Vector3[] vertices = mesh.vertices;
        Matrix4x4 matrix = Matrix4x4.TRS(position, rotation, scale);
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = matrix.MultiplyPoint(vertices[i]);
        }

        return vertices;
    }

    private Vector3 getClosestNormalFromMesh(Vector3 leafMeshVertex, Mesh mesh)
    {
        var goTrans = transform;
        var smallestDistance = 10000000000000f;
        var normal = Vector3.zero;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            var vert = mesh.vertices[i];
            var distance = Vector3.Distance(leafMeshVertex, vert);
            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                normal = mesh.normals[i];
            }
        }

        return normal;
    }
}