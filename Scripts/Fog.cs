﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Fog : MonoBehaviour
{
    public float fogStartDistance = 50f;
    public float fogFadeLength = 40f;
    void Update()
    {
        Shader.SetGlobalFloat("_CustomFog", fogStartDistance);
        Shader.SetGlobalFloat("_CustomFogLength", fogFadeLength);
    }
}
