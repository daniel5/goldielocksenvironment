﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(InstancedGrassCreator))]
public class InstancedGrassCreatorEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        InstancedGrassCreator myScript = (InstancedGrassCreator)target;
        if(GUILayout.Button("Build Grass"))
        {
            myScript.buildGrassMesh();
        }
    }
}