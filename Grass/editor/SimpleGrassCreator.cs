﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class SimpleGrassCreator : MonoBehaviour
{
    public float width = 1;
    public float height = 1;
    public MeshFilter groundPlaneMeshFilter;
    public float maxDistance = 50f;

    public void buildGrassMesh()
    {
        var mf = GetComponent<MeshFilter>();
        var positionMesh = getGrassVetrices();
        var offset = groundPlaneMeshFilter.GetComponent<Transform>().position - transform.position;

        var count = positionMesh.Length;

        var mesh = new Mesh();
        mesh.name = "GrassMesh" + groundPlaneMeshFilter.gameObject.GetInstanceID();
        mf.mesh = mesh;
        var vertices = new Vector3[4 * count];
        var colors = new Color[4 * count];
        var tris = new int[6 * count];
        var normals = new Vector3[4 * count];
        var uv = new Vector2[4 * count];

        Quaternion newRotation = new Quaternion();
        var rotation = 0f;
        for (int i = 0; i < count; i++)
        {
            rotation = Random.Range(-25f, 25f);
            newRotation.eulerAngles = new Vector3(0, rotation, 0);
            var vertexPointer = i * 4;

            var vertPos = Vector3.Lerp(
                              positionMesh[i].vert,
                              positionMesh[Math.Min(i + 1, positionMesh.Length - 1)].vert, Random.Range(0f, 0.9f)) +
                          offset;

            //Create verts
            var randomHeightOffset = Random.Range(0f, 0.5f);
            vertices[vertexPointer] = new Vector3(vertPos.x - width / 2f, vertPos.y, vertPos.z);
            vertices[vertexPointer + 1] = new Vector3(vertPos.x + width / 2f, vertPos.y, vertPos.z);
            vertices[vertexPointer + 2] =
                new Vector3(vertPos.x - width / 2f, vertPos.y + height + height * randomHeightOffset, vertPos.z);
            vertices[vertexPointer + 3] =
                new Vector3(vertPos.x + width / 2f, vertPos.y + height + height * randomHeightOffset, vertPos.z);

            //Create colors
            colors[vertexPointer] = Color.black;
            colors[vertexPointer + 1] = Color.black;
            colors[vertexPointer + 2] = Color.red;
            colors[vertexPointer + 3] = Color.red;

            //Rotate Verts
            vertices[vertexPointer] = newRotation * (vertices[vertexPointer] - vertPos) + vertPos;
            vertices[vertexPointer + 1] = newRotation * (vertices[vertexPointer + 1] - vertPos) + vertPos;
            vertices[vertexPointer + 2] = newRotation * (vertices[vertexPointer + 2] - vertPos) + vertPos;
            vertices[vertexPointer + 3] = newRotation * (vertices[vertexPointer + 3] - vertPos) + vertPos;

            //Create Indices
            var indexPointer = i * 6;
            tris[indexPointer] = 0 + vertexPointer;
            tris[indexPointer + 1] = 2 + vertexPointer;
            tris[indexPointer + 2] = 1 + vertexPointer;
            tris[indexPointer + 3] = 2 + vertexPointer;
            tris[indexPointer + 4] = 3 + vertexPointer;
            tris[indexPointer + 5] = 1 + vertexPointer;

            //Create Normal
            normals[vertexPointer] = positionMesh[i].normal;
            normals[vertexPointer + 1] = positionMesh[i].normal;
            normals[vertexPointer + 2] = positionMesh[i].normal;
            normals[vertexPointer + 3] = positionMesh[i].normal;

            // Create UV
            uv[vertexPointer] = new Vector2(0, 0);
            uv[vertexPointer + 1] = new Vector2(1, 0);
            uv[vertexPointer + 2] = new Vector2(0, 1);
            uv[vertexPointer + 3] = new Vector2(1, 1);
        }

        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.triangles = tris;
        mesh.colors = colors;
        mesh.uv = uv;
        AssetDatabase.CreateAsset(mesh, "Assets/Environment/Grass/GrassMeshes/Resources/" + mesh.name + ".asset");
        AssetDatabase.SaveAssets();
    }

    private VertAndNormal[] getGrassVetrices()
    {
        Transform t = groundPlaneMeshFilter.GetComponent<Transform>();
        List<VertAndNormal> vertices = new List<VertAndNormal>();
        Debug.Log("verts: " + groundPlaneMeshFilter.mesh.vertices.Length + " nm: " +
                  groundPlaneMeshFilter.mesh.normals.Length);
        for (int i = 0; i < groundPlaneMeshFilter.mesh.vertices.Length; i++)
        {
            var worldVertPosition =
                Camera.main.WorldToViewportPoint(t.TransformPoint(groundPlaneMeshFilter.mesh.vertices[i]));
            var distance = Vector3.Distance(t.TransformPoint(groundPlaneMeshFilter.mesh.vertices[i]),
                Camera.main.transform.position);

            if (groundPlaneMeshFilter.mesh.colors[i].b < 0.2f && groundPlaneMeshFilter.mesh.colors[i].g < 0.2f &&
                distance < maxDistance &&
                worldVertPosition.x < 1.1f && worldVertPosition.x > -0-1f &&
                worldVertPosition.y < 1.1f && worldVertPosition.y > -0.1f &&
                worldVertPosition.z > -0.7f)
            {
                vertices.Add(new VertAndNormal(groundPlaneMeshFilter.mesh.vertices[i],
                    groundPlaneMeshFilter.mesh.normals[i]));
            }
        }
        return vertices.ToArray();
    }

    class VertAndNormal
    {
        public VertAndNormal(Vector3 vert, Vector3 normal)
        {
            this.vert = vert;
            this.normal = normal;
        }

        public Vector3 vert { get; }

        public Vector3 normal { get; }
    }
}