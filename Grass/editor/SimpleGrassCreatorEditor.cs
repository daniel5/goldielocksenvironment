﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SimpleGrassCreator))]
public class SimpleGrassCreatorEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SimpleGrassCreator myScript = (SimpleGrassCreator)target;
        if(GUILayout.Button("Build Grass"))
        {
            myScript.buildGrassMesh();
        }
    }
}