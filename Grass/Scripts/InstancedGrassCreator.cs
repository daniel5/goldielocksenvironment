﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class InstancedGrassCreator : MonoBehaviour
{
    public MeshFilter groundPlaneMeshFilter;
    public float maxDistance = 150f;

    public GameObject grassPrefab;
    public GameObject grassPrefab1;
    public GameObject flowerPrefab;
    public GameObject flowerPrefab1;

    private List<Vector3> exisiting = new List<Vector3>();

    public void buildGrassMesh()
    {
        buildFlowerOrGrass(true, grassPrefab, grassPrefab1);
        buildFlowerOrGrass(false, flowerPrefab, flowerPrefab1);
    }

    public void buildFlowerOrGrass(bool isGrass, GameObject fref1, GameObject fref2)
    {
        exisiting.Clear();

        var positionMesh = getGrassVetrices(isGrass);
        var count = positionMesh.Length;


        for (int i = 0; i < count; i++)
        {
            var vertPos = Vector3.Lerp(
                positionMesh[i].vert,
                positionMesh[Math.Min(i + 1, positionMesh.Length - 1)].vert, Random.Range(0f, 0.9f));

            var grassObject = Instantiate(Random.Range(0f,1f)>0.5 ? fref1 : fref2, vertPos, Quaternion.identity, transform);
            var localPos = grassObject.transform.localPosition;
            localPos += transform.position;
            grassObject.transform.localPosition = localPos;


            grassObject.transform.LookAt(Camera.main.transform);
            var localRot = grassObject.transform.eulerAngles;
            localRot.y += Random.Range(-10f, 10f);
            grassObject.transform.rotation = Quaternion.Euler(localRot);
        }
    }

    private VertAndNormal[] getGrassVetrices(bool isGrass)
    {
        Transform t = groundPlaneMeshFilter.GetComponent<Transform>();
        List<VertAndNormal> vertices = new List<VertAndNormal>();

        for (int i = 0; i < groundPlaneMeshFilter.sharedMesh.vertices.Length; i++)
        {
            if (!exisiting.Contains(groundPlaneMeshFilter.sharedMesh.vertices[i]))
            {
                exisiting.Add(groundPlaneMeshFilter.sharedMesh.vertices[i]);

                var worldVertPosition =
                    Camera.main.WorldToViewportPoint(t.TransformPoint(groundPlaneMeshFilter.sharedMesh.vertices[i]));
                var distance = Vector3.Distance(t.TransformPoint(groundPlaneMeshFilter.sharedMesh.vertices[i]),
                    Camera.main.transform.position);

                var doGrass = isGrass && groundPlaneMeshFilter.sharedMesh.colors[i].b < 0.2f &&
                              groundPlaneMeshFilter.sharedMesh.colors[i].g < 0.2f;

                var doFlowers = !isGrass && groundPlaneMeshFilter.sharedMesh.colors[i].r < 0.2f &&
                                groundPlaneMeshFilter.sharedMesh.colors[i].g < 0.2f;

                var shpuldAdd = distance < maxDistance &&
                                worldVertPosition.x < 1.1f && worldVertPosition.x > -0 - 1f &&
                                worldVertPosition.y < 1.1f && worldVertPosition.y > -0.1f &&
                                worldVertPosition.z > -0.7f;

                if (doGrass && shpuldAdd)
                {
                    vertices.Add(new VertAndNormal(groundPlaneMeshFilter.sharedMesh.vertices[i],
                        groundPlaneMeshFilter.sharedMesh.normals[i]));
                }
                if (doFlowers && shpuldAdd)
                {
                    vertices.Add(new VertAndNormal(groundPlaneMeshFilter.sharedMesh.vertices[i],
                        groundPlaneMeshFilter.sharedMesh.normals[i]));
                }
            }
        }
        return vertices.ToArray();
    }

    class VertAndNormal
    {
        public VertAndNormal(Vector3 vert, Vector3 normal)
        {
            this.vert = vert;
            this.normal = normal;
        }

        public Vector3 vert { get; }

        public Vector3 normal { get; }
    }
}